module gitlab.com/erwinnekketsu/embrio-ecoplay-gateway

go 1.15

require (
	github.com/go-playground/validator/v10 v10.7.0
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.11
	github.com/labstack/echo/v4 v4.4.0
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
)
