package route

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/route/middleware"
)

var middlewareHandler = map[string]echo.MiddlewareFunc{
	"auth":       middleware.CheckToken,
	"auth_admin": middleware.CheckTokenAdmin,
}
