package route

import (
	"github.com/labstack/echo/v4"
	auth "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth/handler"
	content "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content/handler"
	notification "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/notification/handler"
	product "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/product/handler"
	user "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/handler"
)

// Handler endpoint to use it later
type Handler interface {
	Handle(c echo.Context) (err error)
}

var endpoint = map[string]Handler{
	//user
	"register":                  user.NewRegister(),
	"detail_user":               user.NewDetailUser(),
	"account_info":              user.NewAccountInfo(),
	"user_file":                 user.NewUserFile(),
	"update_user":               user.NewUpdateUser(),
	"update_account_info":       user.NewUpdateAccountInfo(),
	"update_user_password":      user.NewUpdateUserPassword(),
	"user_verification":         user.NewUserVerification(),
	"list_nasabah":              user.NewListNasabah(),
	"user_recommendation":       user.NewUserRecommendation(),
	"get_location_code":         user.NewGetLocationCode(),
	"location_list":             user.NewLocationList(),
	"support_supplier_list":     user.NewSupportSupplierList(),
	"create_supplier_recommend": user.NewCreateSupplierRecommend(),
	"detail_business":           user.NewDetailBusiness(),
	"update_business":           user.NewUpdateBusiness(),
	"dashboard_data":            user.NewDashboardData(),
	"list_admin":                user.NewListAdmin(),
	"change_status_admin":       user.NewChangeStatusAdmin(),
	"detail_user_full":          user.NewDetailUserFull(),
	"delete_user":               user.NewDeleteUser(),

	//auth
	"login":       auth.NewLogin(),
	"login_admin": auth.NewLoginAdmin(),

	//Admin
	"register_admin":     user.NewRegisterAdmin(),
	"admin_verification": user.NewAdminVerification(),

	//Product
	"create_product":        product.NewCreateProduct(),
	"update_product":        product.NewUpdateProduct(),
	"list_product":          product.NewListProduct(),
	"get_product":           product.NewGetProduct(),
	"comment_product":       product.NewCommentProduct(),
	"reply_comment_product": product.NewReplyCommentProduct(),
	"list_comment_product":  product.NewListCommentProduct(),
	"list_reply_product":    product.NewListReplyProduct(),
	"delete_comment":        product.NewDeleteComment(),

	//Content
	"create_news":     content.NewCreateNews(),
	"update_news":     content.NewUpdateNews(),
	"list_news":       content.NewListNews(),
	"get_news":        content.NewGetNews(),
	"create_banner":   content.NewCreateBanner(),
	"update_banner":   content.NewUpdateBanner(),
	"list_banner":     content.NewListBanner(),
	"get_banner":      content.NewGetBanner(),
	"create_wa_group": content.NewCreateWa(),
	"update_wa_group": content.NewUpdateWa(),
	"list_wa_group":   content.NewListWa(),
	"get_wa_group":    content.NewGetWa(),
	"delete_wa_group": content.NewDeleteWa(),

	//Notification
	"create_notification": notification.NewCreateNotification(),
	"update_notification": notification.NewUpdateNotification(),
	"list_notification":   notification.NewListNotification(),
	"get_notification":    notification.NewGetNotification(),
	"read_notification":   notification.NewReadNotification(),
}
