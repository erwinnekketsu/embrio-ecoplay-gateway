package middleware

import (
	"context"
	"log"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/status"
)

func CheckTokenAdmin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()
		if ctx == nil {
			ctx = context.Background()
		}

		role := c.QueryParam("role")
		if role != "" && role == "su" {
			return next(c)
		}

		var req pb.CheckTokenRequest
		headerToken := c.Request().Header.Get(echo.HeaderAuthorization)
		req.Token = strings.Replace(headerToken, "Bearer ", "", -1)
		log.Println(req.Token)
		res, err := client.CheckTokenAdmin(ctx, &req)
		log.Println(res)
		if err != nil {
			st, _ := status.FromError(err)
			resp := &util.Response{
				Code:    st.Code(),
				Message: st.Message(),
				Errors:  []string{st.Message()},
			}
			return resp.JSON(c)
		}
		payload := auth.TokenPayload{
			UserID: res.Id,
			Email:  res.Email,
			Status: res.Status,
		}
		// TODO wrapping context
		c.Set(util.ContextTokenValueKey, payload)
		return next(c)
	}
}
