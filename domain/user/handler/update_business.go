package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UpdateBusiness struct{}

func NewUpdateBusiness() *UpdateBusiness {
	return &UpdateBusiness{}
}

func (u *UpdateBusiness) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	tokenPayload, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var r user.BusinessProfileDataRequest
	if err := u.validate(&r, c); err != nil {
		log.Println("validate error : ", err.Error())
		return err
	}

	var req pb.UpdateDetailBusinessRequest
	bytes, _ := json.Marshal(&r)
	_ = json.Unmarshal(bytes, &req)
	req.UserId = tokenPayload.UserID
	req.Id = int32(util.StringToInteger(c.Param("id")))
	log.Println(req.BusinessChain)
	_, err := client.UpdateDetailBusiness(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := u.buildErrorResponse(ctx, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}
	resp, err := u.buildResponse(ctx, c)
	if err != nil {
		return err
	}
	log.Println("response", resp.Data)
	return resp.JSON(c)
}

func (u *UpdateBusiness) buildResponse(ctx context.Context, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.SuccessCreated,
		Message: util.StatusMessage[util.SuccessCreated],
		Data: map[string]interface{}{
			"message": util.StatusMessage[util.SuccessCreated],
		},
	}
	return resp, nil
}

func (u *UpdateBusiness) buildErrorResponse(ctx context.Context, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (u *UpdateBusiness) validate(r *user.BusinessProfileDataRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
