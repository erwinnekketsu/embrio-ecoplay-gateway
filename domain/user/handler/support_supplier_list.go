package handler

import (
	"context"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type SupportSupplierList struct{}

func NewSupportSupplierList() *SupportSupplierList {
	return &SupportSupplierList{}
}

func (h *SupportSupplierList) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var req pb.SupportSupplierListRequest
	req.Page = int32(util.StringToInteger(c.QueryParam("page")))
	req.Status = int32(util.StringToInteger(c.QueryParam("status")))
	req.Limit = int32(util.StringToInteger(c.QueryParam("limit")))
	req.Query = c.QueryParam("query")
	grpcResp, err := client.SupportSupplierList(ctx, &req)
	if err != nil {
		log.Printf("[Ecoplay]  error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[Ecoplay]  error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *SupportSupplierList) buildResponse(ctx context.Context, response *pb.SupportSupplierListResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"suppliers": response.SupportSupplier,
		},
	}
	return resp, nil
}

func (h *SupportSupplierList) buildErrorResponse(ctx context.Context, response *pb.SupportSupplierListResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *SupportSupplierList) validate(r *user.SupportSupplierListRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
