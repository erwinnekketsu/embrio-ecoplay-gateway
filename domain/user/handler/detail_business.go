package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DetailBusiness struct{}

func NewDetailBusiness() *DetailBusiness {
	return &DetailBusiness{}
}

func (du *DetailBusiness) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	tokenPayload, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.DetailBusinessRequest
	req.UserId = tokenPayload.UserID
	req.Id = int32(util.StringToInteger(c.Param("id")))

	grpcResp, err := client.DetailBusiness(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := du.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}

	var detailBusiness user.BusinessProfileData
	bytes, _ := json.Marshal(&grpcResp)
	json.Unmarshal(bytes, &detailBusiness)

	resp, err := du.buildResponse(ctx, detailBusiness, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (du *DetailBusiness) buildResponse(ctx context.Context, response user.BusinessProfileData, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"business_data": response,
		},
	}
	return resp, nil
}

func (du *DetailBusiness) buildErrorResponse(ctx context.Context, response *pb.DetailBusinessResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (du *DetailBusiness) validate(r *auth.CheckTokenRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
