package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AccountInfo struct{}

func NewAccountInfo() *AccountInfo {
	return &AccountInfo{}
}

func (ai *AccountInfo) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	tokenPayload, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.AccountInfoRequest
	req.Id = tokenPayload.UserID

	grpcResp, err := client.AccountInfo(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := ai.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}

	var accountInfo user.AccountInfoResponse
	bytes, _ := json.Marshal(&grpcResp)
	json.Unmarshal(bytes, &accountInfo)

	resp, err := ai.buildResponse(ctx, accountInfo, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (ai *AccountInfo) buildResponse(ctx context.Context, response user.AccountInfoResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"account_info": response,
		},
	}
	return resp, nil
}

func (ai *AccountInfo) buildErrorResponse(ctx context.Context, response *pb.AccountInfoResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (ai *AccountInfo) validate(r *auth.CheckTokenRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
