package handler

import (
	"context"
	"encoding/json"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Register struct{}

func (h *Register) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var r user.RegisterRequest

	if err := h.validate(&r, c); err != nil {
		log.Println("validate error : ", err.Error())
		return err
	}

	var req pb.RegisterRequest
	bytes, _ := json.Marshal(&r)
	_ = json.Unmarshal(bytes, &req)

	grpcResp, err := client.Register(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := h.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (h *Register) buildResponse(ctx context.Context, response *pb.RegisterResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.SuccessCreated,
		Message: util.StatusMessage[util.SuccessCreated],
		Data: map[string]interface{}{
			"token": response.Token,
		},
	}
	return resp, nil
}

func (h *Register) buildErrorResponse(ctx context.Context, response *pb.RegisterResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *Register) validate(r *user.RegisterRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}

func NewRegister() *Register {
	return &Register{}
}
