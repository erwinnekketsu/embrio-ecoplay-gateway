package handler

import (
	"context"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeleteUser struct{}

func NewDeleteUser() *DeleteUser {
	return &DeleteUser{}
}

func (du *DeleteUser) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.DetailUserRequest
	id := c.Param("id")
	req.Id = int32(util.StringToInteger(id))

	_, err := client.DeleteUserRegister(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := du.buildErrorResponse(ctx, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}

	resp, err := du.buildResponse(ctx, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (du *DeleteUser) buildResponse(ctx context.Context, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.SuccessCreated,
		Message: util.StatusMessage[util.SuccessCreated],
		Data: map[string]interface{}{
			"message": util.StatusMessage[util.SuccessCreated],
		},
	}
	return resp, nil
}

func (du *DeleteUser) buildErrorResponse(ctx context.Context, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (du *DeleteUser) validate(r *user.UpdateUserRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
