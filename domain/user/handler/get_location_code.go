package handler

import (
	"context"
	"encoding/json"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetLocationCode struct{}

func NewGetLocationCode() *GetLocationCode {
	return &GetLocationCode{}
}

func (du *GetLocationCode) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var req pb.GetLocationCodeRequest
	req.Postcode = int32(util.StringToInteger(c.QueryParam("postcode")))

	grpcResp, err := client.GetLocationCode(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := du.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}

	var GetLocationCode user.LocationId
	bytes, _ := json.Marshal(&grpcResp)
	json.Unmarshal(bytes, &GetLocationCode)

	resp, err := du.buildResponse(ctx, GetLocationCode, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (du *GetLocationCode) buildResponse(ctx context.Context, response user.LocationId, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"location_code": response,
		},
	}
	return resp, nil
}

func (du *GetLocationCode) buildErrorResponse(ctx context.Context, response *pb.GetLocationCodeResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (du *GetLocationCode) validate(r *auth.CheckTokenRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
