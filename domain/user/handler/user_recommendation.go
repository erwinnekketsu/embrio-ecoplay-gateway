package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	sUser "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type UserRecommendation struct{}

func NewUserRecommendation() *UserRecommendation {
	return &UserRecommendation{}
}

func (h *UserRecommendation) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	tokenPayload, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.UserRecommendationRequest
	req.Id = int64(tokenPayload.UserID)
	req.Tag = c.QueryParam("tag")
	grpcResp, err := client.UserRecommendation(ctx, &req)
	if err != nil {
		log.Printf("[Ecoplay]  error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[Ecoplay]  error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *UserRecommendation) buildResponse(ctx context.Context, response *pb.UserRecommendationResponse, c echo.Context) (*util.Response, error) {
	var userRecommendation sUser.UserRecommendationDefault
	bytes, _ := json.Marshal(&response)
	json.Unmarshal(bytes, &userRecommendation)
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"user_recommendation": userRecommendation.UserRecommendation,
		},
	}
	return resp, nil
}

func (h *UserRecommendation) buildErrorResponse(ctx context.Context, response *pb.UserRecommendationResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *UserRecommendation) validate(r *user.UserRecommendationRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
