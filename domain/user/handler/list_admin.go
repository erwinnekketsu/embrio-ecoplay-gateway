package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	sUser "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type ListAdmin struct{}

func NewListAdmin() *ListAdmin {
	return &ListAdmin{}
}

func (h *ListAdmin) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.ListAdminRequest
	req.Page = int32(util.StringToInteger(c.QueryParam("page")))
	req.Status = int32(util.StringToInteger(c.QueryParam("status")))
	req.Limit = int32(util.StringToInteger(c.QueryParam("limit")))
	grpcResp, err := client.ListAdmin(ctx, &req)
	if err != nil {
		log.Printf("[Ecoplay] ListAdmin error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[Ecoplay] ListAdmin error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *ListAdmin) buildResponse(ctx context.Context, response *pb.ListAdminResponse, c echo.Context) (*util.Response, error) {
	var listAdmin sUser.AdminDefault
	bytes, _ := json.Marshal(&response)
	json.Unmarshal(bytes, &listAdmin)
	var resp *util.Response
	if listAdmin.Admin == nil {
		resp = &util.Response{
			Code:    util.Success,
			Message: util.StatusMessage[util.Success],
			Data: map[string]interface{}{
				"admin":      []string{},
				"pagination": listAdmin.Pagination,
			},
		}
	} else {
		resp = &util.Response{
			Code:    util.Success,
			Message: util.StatusMessage[util.Success],
			Data: map[string]interface{}{
				"admin":      listAdmin.Admin,
				"pagination": listAdmin.Pagination,
			},
		}
	}

	return resp, nil
}

func (h *ListAdmin) buildErrorResponse(ctx context.Context, response *pb.ListAdminResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *ListAdmin) validate(r *user.ListAdminRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
