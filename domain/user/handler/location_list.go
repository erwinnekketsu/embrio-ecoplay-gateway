package handler

import (
	"context"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type LocationList struct{}

func NewLocationList() *LocationList {
	return &LocationList{}
}

func (h *LocationList) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var req pb.LocationListRequest
	req.Id = int32(util.StringToInteger(c.QueryParam("id")))
	req.Tag = c.QueryParam("tag")
	grpcResp, err := client.LocationList(ctx, &req)
	if err != nil {
		log.Printf("[Ecoplay]  error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[Ecoplay]  error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *LocationList) buildResponse(ctx context.Context, response *pb.LocationListResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"location_list": response.Location,
		},
	}
	return resp, nil
}

func (h *LocationList) buildErrorResponse(ctx context.Context, response *pb.LocationListResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *LocationList) validate(r *user.LocationListRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
