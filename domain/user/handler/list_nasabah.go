package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	sUser "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type ListNasabah struct{}

func NewListNasabah() *ListNasabah {
	return &ListNasabah{}
}

func (h *ListNasabah) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.ListNasabahRequest
	req.Page = int32(util.StringToInteger(c.QueryParam("page")))
	req.Status = int32(util.StringToInteger(c.QueryParam("status")))
	req.Limit = int32(util.StringToInteger(c.QueryParam("limit")))
	req.Query = c.QueryParam("query")
	grpcResp, err := client.ListNasabah(ctx, &req)
	if err != nil {
		log.Printf("[Ecoplay] ListNasabah error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[Ecoplay] ListNasabah error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *ListNasabah) buildResponse(ctx context.Context, response *pb.ListNasabahResponse, c echo.Context) (*util.Response, error) {
	var listNasabah sUser.NasabahDefault
	bytes, _ := json.Marshal(&response)
	json.Unmarshal(bytes, &listNasabah)
	var resp *util.Response
	if listNasabah.Nasabah == nil {
		resp = &util.Response{
			Code:    util.Success,
			Message: util.StatusMessage[util.Success],
			Data: map[string]interface{}{
				"nasabah":    []string{},
				"pagination": listNasabah.Pagination,
			},
		}
	} else {
		resp = &util.Response{
			Code:    util.Success,
			Message: util.StatusMessage[util.Success],
			Data: map[string]interface{}{
				"nasabah":    listNasabah.Nasabah,
				"pagination": listNasabah.Pagination,
			},
		}
	}

	return resp, nil
}

func (h *ListNasabah) buildErrorResponse(ctx context.Context, response *pb.ListNasabahResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *ListNasabah) validate(r *user.ListNasabahRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
