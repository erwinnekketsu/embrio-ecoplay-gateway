package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/user/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DetailUserFull struct{}

func NewDetailUserFull() *DetailUserFull {
	return &DetailUserFull{}
}

func (du *DetailUserFull) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.DetailUserRequest
	id := c.Param("id")
	req.Id = int32(util.StringToInteger(id))

	grpcResp, err := client.DetailUserFull(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := du.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}

	var detailUserFull user.DetailUserFullResponse
	bytes, _ := json.Marshal(&grpcResp)
	json.Unmarshal(bytes, &detailUserFull)

	resp, err := du.buildResponse(ctx, detailUserFull, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (du *DetailUserFull) buildResponse(ctx context.Context, response user.DetailUserFullResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"user_data": response,
		},
	}
	return resp, nil
}

func (du *DetailUserFull) buildErrorResponse(ctx context.Context, response *pb.DetailUserFullResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (du *DetailUserFull) validate(r *auth.CheckTokenRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
