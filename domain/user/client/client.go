package client

import (
	"context"
	"os"

	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/user"
	Grpc "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
)

// Register client handler
func Register(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.Register(ctx, req)
}

// DetailUser client handler
func DetailUser(ctx context.Context, req *pb.DetailUserRequest) (*pb.DetailUserResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.DetailUser(ctx, req)
}

// AccountInfo client handler
func AccountInfo(ctx context.Context, req *pb.AccountInfoRequest) (*pb.AccountInfoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.AccountInfo(ctx, req)
}

// UserFile client handler
func UserFile(ctx context.Context, req *pb.UserFileRequest) (*pb.UserFileResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.UserFile(ctx, req)
}

// UpdateUser client handler
func UpdateUser(ctx context.Context, req *pb.UpdateUserRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.UpdateUser(ctx, req)
}

// UpdateAccountInfo client handler
func UpdateAccountInfo(ctx context.Context, req *pb.UpdateAccountInfoRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.UpdateAccountInfo(ctx, req)
}

// UpdateUserPassword client handler
func UpdateUserPassword(ctx context.Context, req *pb.UpdateUserPasswordRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.UpdateUserPassword(ctx, req)
}

// RegisterAdmin client handler
func RegisterAdmin(ctx context.Context, req *pb.RegisterAdminRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.RegisterAdmin(ctx, req)
}

// AdminVerification client handler
func AdminVerification(ctx context.Context, req *pb.AdminVerificationRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.AdminVerification(ctx, req)
}

// UserVerification client handler
func UserVerification(ctx context.Context, req *pb.UserVerificationRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.UserVerification(ctx, req)
}

// ListNasabah client handler
func ListNasabah(ctx context.Context, req *pb.ListNasabahRequest) (*pb.ListNasabahResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.ListNasabah(ctx, req)
}

// UserRecommendation client handler
func UserRecommendation(ctx context.Context, req *pb.UserRecommendationRequest) (*pb.UserRecommendationResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.UserRecommendation(ctx, req)
}

// GetLocationCode client handler
func GetLocationCode(ctx context.Context, req *pb.GetLocationCodeRequest) (*pb.GetLocationCodeResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.GetLocationCode(ctx, req)
}

// LocationList client handler
func LocationList(ctx context.Context, req *pb.LocationListRequest) (*pb.LocationListResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.LocationList(ctx, req)
}

// SupportSupplierList client handler
func SupportSupplierList(ctx context.Context, req *pb.SupportSupplierListRequest) (*pb.SupportSupplierListResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.SupportSupplierList(ctx, req)
}

// CreateSupplierRecommend client handler
func CreateSupplierRecommend(ctx context.Context, req *pb.CreateSupplierRecommendRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.CreateSupplierRecommend(ctx, req)
}

// DetailBusiness client handler
func DetailBusiness(ctx context.Context, req *pb.DetailBusinessRequest) (*pb.DetailBusinessResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.DetailBusiness(ctx, req)
}

// UpdateDetailBusiness client handler
func UpdateDetailBusiness(ctx context.Context, req *pb.UpdateDetailBusinessRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.UpdateDetailBusiness(ctx, req)
}

// DashboardData client handler
func DashboardData(ctx context.Context, req *pb.DashboardDataRequest) (*pb.DashboardDataResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.DashboardData(ctx, req)
}

// ListAdmin client handler
func ListAdmin(ctx context.Context, req *pb.ListAdminRequest) (*pb.ListAdminResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.ListAdmin(ctx, req)
}

// ChangeStatusAdmin client handler
func ChangeStatusAdmin(ctx context.Context, req *pb.ChangeStatusAdminRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.ChangeStatusAdmin(ctx, req)
}

// DetailUserFull client handler
func DetailUserFull(ctx context.Context, req *pb.DetailUserRequest) (*pb.DetailUserFullResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.DetailUserFull(ctx, req)
}

// DeleteUserRegister client handler
func DeleteUserRegister(ctx context.Context, req *pb.DetailUserRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("user_grpc"))
	defer conn.Close()

	client := pb.NewUserClient(conn)

	return client.DeleteUserRegister(ctx, req)
}
