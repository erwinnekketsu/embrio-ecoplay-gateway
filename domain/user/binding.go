package user

type (
	DetailUserResponse struct {
		Fullname       string `default:"" json:"fullname,omitempty"`
		Username       string `default:"" json:"username,omitempty"`
		PhoneNumber    string `default:"" json:"phone_number,omitempty"`
		Email          string `default:"" json:"email,omitempty"`
		AccountNumber  string `default:"" json:"account_number,omitempty"`
		IdNumber       int64  `default:"0" json:"id_number"`
		WhatsappNumber string `default:"" json:"whatsapp_number"`
		Foto           string `default:"" json:"foto"`
	}

	DetailUserFullResponse struct {
		Fullname       string `json:"fullname"`
		Username       string `json:"username"`
		PhoneNumber    string `json:"phone_number"`
		Email          string `json:"email"`
		AccountNumber  string `json:"account_number"`
		IDNumber       string `json:"id_number"`
		WhatsappNumber string `json:"whatsapp_number"`
		Foto           string `json:"foto"`
		UserFile       struct {
			Siup        string `json:"siup"`
			Tdp         string `json:"tdp"`
			Npwp        string `json:"npwp"`
			SupportFile string `json:"support_file"`
		} `json:"user_file"`
		AccountInfo struct {
			BankAccount   string `json:"bank_account"`
			AccountNumber string `json:"account_number"`
			AccountName   string `json:"account_name"`
		} `json:"accountInfo"`
		DetailBusiness struct {
			ID                  int    `json:"id"`
			UserID              int    `json:"user_id"`
			BusinessName        string `json:"business_name"`
			BusinessDescription string `json:"business_description"`
			BusinessType        string `json:"business_type"`
			BusinessChain       string `json:"business_chain"`
			BusinessPicture     string `json:"business_picture"`
			TotalEmployee       int    `json:"total_employee"`
			MonthlyIncome       int    `json:"monthly_income"`
			Website             string `json:"website"`
			Facebook            string `json:"facebook"`
			Instagram           string `json:"instagram"`
			Shopee              string `json:"shopee"`
			Tokopedia           string `json:"tokopedia"`
			Bukalapak           string `json:"bukalapak"`
		} `json:"detailBusiness"`
		BusinessData struct {
			BusinessName     string `json:"business_name"`
			BusinessType     string `json:"business_type"`
			BusinessAddress  string `json:"business_address"`
			Province         int    `json:"province"`
			CityDistirct     int    `json:"city_distirct"`
			District         int    `json:"district"`
			Postcode         int    `json:"postcode"`
			Long             string `json:"long"`
			Lat              string `json:"lat"`
			ProvinceText     string `json:"province_text"`
			CityDistrictText string `json:"city_district_text"`
			DistrictText     string `json:"district_text"`
			PinLocation      string `json:"pin_location"`
		} `json:"business_data"`
	}

	AccountInfoResponse struct {
		AccountNumber string `default:"" json:"account_number"`
		AccountName   string `default:"" json:"account_name"`
		BankName      string `default:"" json:"bank_account"`
	}

	UserFileResponse struct {
		Siup        string `default:"" json:"siup"`
		Tdp         string `default:"" json:"tdp"`
		Npwp        string `default:"" json:"npwp"`
		SupportFile string `default:"" json:"support_file"`
	}

	BusinessProfileData struct {
		ID                  int32  `json:"id" default:""`
		UserID              int32  `json:"user_id" default:""`
		BusinessName        string `json:"business_name" default:""`
		BusinessDescription string `json:"business_description" default:""`
		BusinessType        string `json:"business_type" default:""`
		BusinessChain       string `json:"business_chain" default:""`
		TotalEmployee       int32  `json:"total_employee" default:""`
		MonthlyIncome       int32  `json:"monthly_income" default:""`
		BusinessPicture     string `json:"business_picture" default:""`
		Website             string `json:"website" default:""`
		Facebook            string `json:"facebook" default:""`
		Instagram           string `json:"instagram" default:""`
		Shopee              string `json:"shopee" default:""`
		Tokopedia           string `json:"tokopedia" default:""`
		Bukalapak           string `json:"bukalapak" default:""`
	}

	DashboardData struct {
		TotalStore   int64     `json:"total_store"`
		TotalProduct int64     `json:"total_product"`
		TotalUser    int64     `json:"total_user"`
		DateStore    []string  `json:"date_store"`
		DateProduct  []string  `json:"date_product"`
		DateUser     []string  `json:"date_user"`
		DataStore    []float32 `json:"data_store"`
		DataProduct  []float32 `json:"data_product"`
		DataUser     []float32 `json:"data_user"`
	}
)
