package user

type (
	RegisterRequest struct {
		UserData struct {
			Fullname      string `json:"fullname"`
			Username      string `json:"username"`
			PhoneNumber   string `json:"phone_number"`
			Email         string `json:"email"`
			AccountNumber string `json:"account_number"`
			Password      string `json:"password"`
			Timezone      string `json:"timezone"`
		} `json:"user_data"`
		BusinessData struct {
			BusinessName    string `json:"business_name"`
			BusinessType    string `json:"business_type"`
			BusinessAddress string `json:"business_address"`
			Province        int    `json:"province"`
			CityDistirct    int    `json:"city_distirct"`
			District        int    `json:"district"`
			Postcode        int    `json:"postcode"`
		} `json:"business_data"`
		UserFile struct {
			Siup        string `json:"siup"`
			Tdp         string `json:"tdp"`
			Npwp        string `json:"npwp"`
			SupportFile string `json:"support_file"`
		} `json:"user_file"`
	}

	UpdateUserRequest struct {
		Fullname    string `json:"fullname" `
		Username    string `json:"username" `
		PhoneNumber string `json:"phone_number" `
		Email       string `json:"email" `
		IdNumber    int64  `json:"id_number" `
		WANumber    string `json:"whatsapp_number" `
		Foto        string `json:"foto" `
		ID          int    `json:"id" `
	}

	UpdateAccountInfoRequest struct {
		UserID        int    `json:"User_id" `
		AccountNumber string `json:"account_number"`
		AccountName   string `json:"account_name"`
		BankAccount   string `json:"bank_account"`
	}

	UpdateUserPasswordRequest struct {
		UserID      int    `json:"user_id" `
		Password    string `json:"password"`
		NewPassword string `json:"nwe_password"`
	}

	RegisterAdminRequest struct {
		Username string `json:"username"`
		Email    string `json:"email"`
		FullName string `json:"full_name"`
		Password string `json:"password"`
		Role     int32  `json:"role"`
	}

	UserVerificationRequest struct {
		ID int `json:"id" `
	}

	ChangeStatusAdminRequest struct {
		ID     int32 `json:"id" `
		Status int32 `json:"status" `
	}

	UserAndBusiness struct {
		ID            int    `json:"id"`
		BusinessName  string `json:"business_name"`
		BusinessType  string `json:"business_type"`
		BusinessChain string `json:"business_chain"`
		Province      string `json:"province"`
	}

	LocationId struct {
		ProvinceId int32 `json:"province_id"`
		CityId     int32 `json:"city_id"`
		DistrictId int32 `json:"district_id"`
	}

	Location struct {
		Id   int32  `json:"id"`
		Name string `json:"name"`
	}

	SupportSupplier struct {
		ID           int32  `json:"id"`
		UserId       int32  `json:"user_id"`
		BusinessName string `json:"business_name"`
		Description  string `json:"description"`
	}

	SupplierRecommend struct {
		UserId     int32   `json:"user_id"`
		SupplierId []int32 `json:"supplier_id"`
	}

	BusinessProfileDataRequest struct {
		ID                  int32  `json:"id"`
		UserID              int32  `json:"user_id"`
		BusinessName        string `json:"business_name"`
		BusinessDescription string `json:"business_description"`
		BusinessType        string `json:"business_type"`
		BusinessChain       string `json:"business_chain"`
		TotalEmployee       int32  `json:"total_employee"`
		MonthlyIncome       int32  `json:"monthly_income"`
		BusinessPicture     string `json:"business_picture"`
		Website             string `json:"website"`
		Facebook            string `json:"facebook"`
		Instagram           string `json:"instagram"`
		Shopee              string `json:"shopee"`
		Tokopedia           string `json:"tokopedia"`
		Bukalapak           string `json:"bukalapak"`
	}

	NasabahDefault struct {
		Nasabah []struct {
			ID            int    `json:"id"`
			Name          string `json:"name"`
			BusinessName  string `json:"business_name"`
			BusinessType  string `json:"business_type"`
			BusinessChain string `json:"business_chain"`
			Province      string `json:"province"`
			Status        int    `json:"status"`
		} `json:"nasabah"`
		Pagination struct {
			PageSize     int `json:"page_size"`
			CurrentPage  int `json:"current_page"`
			TotalPage    int `json:"total_page"`
			TotalResults int `json:"total_results"`
		} `json:"pagination"`
	}

	AdminDefault struct {
		Admin []struct {
			ID       int    `json:"id"`
			Username string `json:"username"`
			Email    string `json:"email"`
			FullName string `json:"full_name"`
			Status   int    `json:"status"`
		} `json:"admin"`
		Pagination struct {
			PageSize     int `json:"page_size"`
			CurrentPage  int `json:"current_page"`
			TotalPage    int `json:"total_page"`
			TotalResults int `json:"total_results"`
		} `json:"pagination"`
	}

	UserRecommendationDefault struct {
		UserRecommendation []struct {
			UserID       int    `json:"user_id"`
			Fullname     string `json:"fullname"`
			BusinessName string `json:"business_name"`
			Address      string `json:"address"`
			Long         int    `json:"long"`
			Lat          int    `json:"lat"`
			Distance     int    `json:"distance"`
			DistanceText string `json:"distance_text"`
			ID           int    `json:"id"`
			City         string `json:"city"`
			Image        string `json:"image"`
			SupplierName string `json:"supplier_name"`
			Price        int    `json:"price"`
		} `json:"user_recommendation"`
	}
)
