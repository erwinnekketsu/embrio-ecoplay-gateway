package handler

import (
	"context"
	"encoding/json"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type LoginAdmin struct{}

func (h *LoginAdmin) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	r := new(auth.LoginRequest)
	err := h.validate(r, c)
	if err != nil {
		log.Println("validate error : ", err.Error())
		return err
	}

	var req pb.LoginRequest
	bytes, _ := json.Marshal(&r)
	_ = json.Unmarshal(bytes, &req)

	log.Println("username ", req.GetUsername())
	log.Println("password ", req.GetPassword())
	grpcResp, err := client.LoginAdmin(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := h.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (h *LoginAdmin) buildResponse(ctx context.Context, response *pb.LoginResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"token": response.Token,
			"name":  response.Name,
			"role":  response.Role,
		},
	}
	return resp, nil
}

func (h *LoginAdmin) buildErrorResponse(ctx context.Context, response *pb.LoginResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *LoginAdmin) validate(r *auth.LoginRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}

func NewLoginAdmin() *LoginAdmin {
	return &LoginAdmin{}
}
