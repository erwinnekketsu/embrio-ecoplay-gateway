package auth

// LoginRequest struct
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Token    string `json:"token"`
}

// CheckTokenRequest struct
type CheckTokenRequest struct {
	Token string `json:"token"`
}

// TokenPayload struct
type TokenPayload struct {
	UserID int32  `json:"id"`
	Email  string `json:"email"`
	Status int32  `json:"status"`
}
