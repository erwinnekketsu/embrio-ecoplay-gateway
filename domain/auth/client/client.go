package client

import (
	"context"
	"os"

	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/auth"
	Grpc "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
)

// Login client handler
func Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	conn := Grpc.Dial(os.Getenv("auth_grpc"))
	defer conn.Close()

	client := pb.NewAuthClient(conn)

	return client.Login(ctx, req)
}

// Login client handler
func CheckToken(ctx context.Context, req *pb.CheckTokenRequest) (*pb.CheckTokenResponse, error) {
	conn := Grpc.Dial(os.Getenv("auth_grpc"))
	defer conn.Close()

	client := pb.NewAuthClient(conn)

	return client.CheckToken(ctx, req)
}

// LoginAdmin client handler
func LoginAdmin(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	conn := Grpc.Dial(os.Getenv("auth_grpc"))
	defer conn.Close()

	client := pb.NewAuthClient(conn)

	return client.LoginAdmin(ctx, req)
}

// CheckTokenAdmin client handler
func CheckTokenAdmin(ctx context.Context, req *pb.CheckTokenRequest) (*pb.CheckTokenResponse, error) {
	conn := Grpc.Dial(os.Getenv("auth_grpc"))
	defer conn.Close()

	client := pb.NewAuthClient(conn)

	return client.CheckTokenAdmin(ctx, req)
}
