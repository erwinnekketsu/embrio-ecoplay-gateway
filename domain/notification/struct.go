package content

type Notification struct {
	ID           int32  `json:"id"`
	IdProduk     int32  `json:"product_id"`
	IdNotifikasi int32  `json:"notification_id"`
	From         int32  `json:"from"`
	To           int32  `json:"to"`
	Title        string `json:"title"`
	Content      string `json:"content"`
}
