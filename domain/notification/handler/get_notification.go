package handler

import (
	"context"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/notification/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/notification"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type GetNotification struct{}

func NewGetNotification() *GetNotification {
	return &GetNotification{}
}

func (h *GetNotification) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.GetNotificationRequest
	req.Id = int32(util.StringToInteger(c.Param("id")))
	grpcResp, err := client.GetNotification(ctx, &req)
	if err != nil {
		log.Printf("[POS Modifier] ListModifier error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[POS Modifier] ListModifier error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *GetNotification) buildResponse(ctx context.Context, response *pb.GetNotificationResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"notification": response.Notifications,
		},
	}
	return resp, nil
}

func (h *GetNotification) buildErrorResponse(ctx context.Context, response *pb.GetNotificationResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}
