package client

import (
	"context"
	"os"

	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/product"
	Grpc "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
)

// ListProduct client handler
func ListProduct(ctx context.Context, req *pb.ListProductRequest) (*pb.ListProductResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.ListProduct(ctx, req)
}

// GetProduct client handler
func GetProduct(ctx context.Context, req *pb.GetProductRequest) (*pb.GetProductResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.GetProduct(ctx, req)
}

// CreateProduct client handler
func CreateProduct(ctx context.Context, req *pb.CreateProductRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.CreateProduct(ctx, req)
}

// UpdateProduct client handler
func UpdateProduct(ctx context.Context, req *pb.UpdateProductRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.UpdateProduct(ctx, req)
}

// CommentProduct client handler
func CommentProduct(ctx context.Context, req *pb.CommentProductRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.CommentProduct(ctx, req)
}

// ReplyCommentProduct client handler
func ReplyCommentProduct(ctx context.Context, req *pb.ReplyCommentProductRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.ReplyCommentProduct(ctx, req)
}

// ListCommentProduct client handler
func ListCommentProduct(ctx context.Context, req *pb.ListCommentProductRequest) (*pb.ListCommentProductResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.ListCommentProduct(ctx, req)
}

// ListReplyProduct client handler
func ListReplyProduct(ctx context.Context, req *pb.ListReplyProductRequest) (*pb.ListCommentProductResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.ListReplyProduct(ctx, req)
}

// DeleteComment client handler
func DeleteComment(ctx context.Context, req *pb.DeleteCommentRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("product_grpc"))
	defer conn.Close()

	client := pb.NewProductClient(conn)

	return client.DeleteComment(ctx, req)
}
