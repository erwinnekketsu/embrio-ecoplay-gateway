package handler

import (
	"context"
	"encoding/json"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/product"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/product/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/product"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UpdateProduct struct{}

func NewUpdateProduct() *UpdateProduct {
	return &UpdateProduct{}
}

func (h *UpdateProduct) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	r := new(product.Product)
	err := h.validate(r, c)
	if err != nil {
		log.Println("validate error : ", err.Error())
		return err
	}

	var req pb.UpdateProductRequest
	bytes, _ := json.Marshal(&r)
	_ = json.Unmarshal(bytes, &req)
	req.Id = int32(util.StringToInteger(c.Param("id")))

	grpcResp, err := client.UpdateProduct(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := h.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (h *UpdateProduct) buildResponse(ctx context.Context, response *pb.NoResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
	}
	return resp, nil
}

func (h *UpdateProduct) buildErrorResponse(ctx context.Context, response *pb.NoResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *UpdateProduct) validate(r *product.Product, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
