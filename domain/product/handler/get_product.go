package handler

import (
	"context"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/product/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/product"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/product"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type GetProduct struct{}

func NewGetProduct() *GetProduct {
	return &GetProduct{}
}

func (h *GetProduct) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var req pb.GetProductRequest
	req.Id = int32(util.StringToInteger(c.Param("id")))
	grpcResp, err := client.GetProduct(ctx, &req)
	if err != nil {
		log.Printf("[POS Modifier] ListModifier error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[POS Modifier] ListModifier error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *GetProduct) buildResponse(ctx context.Context, response *pb.GetProductResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"product": response.Products,
		},
	}
	return resp, nil
}

func (h *GetProduct) buildErrorResponse(ctx context.Context, response *pb.GetProductResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *GetProduct) validate(r *product.GetProductRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
