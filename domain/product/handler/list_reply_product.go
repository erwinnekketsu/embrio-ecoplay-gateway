package handler

import (
	"context"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/product/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/product"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/product"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type ListReplyProduct struct{}

func NewListReplyProduct() *ListReplyProduct {
	return &ListReplyProduct{}
}

func (h *ListReplyProduct) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	var req pb.ListReplyProductRequest
	req.CommentId = int64(util.StringToInteger(c.Param("comment_id")))
	req.Page = int32(util.StringToInteger(c.QueryParam("page")))
	req.Limit = int32(util.StringToInteger(c.QueryParam("limit")))
	grpcResp, err := client.ListReplyProduct(ctx, &req)
	if err != nil {
		log.Printf("[POS Modifier] ListModifier error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[POS Modifier] ListModifier error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *ListReplyProduct) buildResponse(ctx context.Context, response *pb.ListCommentProductResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"comments":   response.Comments,
			"pagination": response.Pagination,
		},
	}
	return resp, nil
}

func (h *ListReplyProduct) buildErrorResponse(ctx context.Context, response *pb.ListCommentProductResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *ListReplyProduct) validate(r *product.ListReplyProductRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
