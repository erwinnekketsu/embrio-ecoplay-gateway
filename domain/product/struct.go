package product

type (
	Product struct {
		ID          int32   `json:"id"`
		Nama        string  `json:"nama"`
		Description string  `json:"description"`
		Category    string  `json:"category"`
		Price       float32 `json:"price"`
		Stock       int32   `json:"stock"`
		Unit        string  `json:"unit"`
		Image       string  `json:"image"`
		BusinessId  int32   `json:"business_id"`
	}

	CommentProduct struct {
		ID          int32  `json:"id"`
		Name        string `json:"name"`
		UserId      int64  `json:"user_id"`
		ProductId   int64  `json:"product_id"`
		Description string `json:"description"`
		Date        string `json:"date"`
	}

	ReplyCommentProduct struct {
		ID          int32  `json:"id"`
		CommentId   int64  `json:"comment_id"`
		Name        string `json:"name"`
		UserId      int64  `json:"user_id"`
		ProductId   int64  `json:"product_id"`
		Description string `json:"description"`
		Date        string `json:"date"`
	}
)
