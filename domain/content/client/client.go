package client

import (
	"context"
	"os"

	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	Grpc "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
)

// CreateNews client handler
func CreateNews(ctx context.Context, req *pb.CreateNewsRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.CreateNews(ctx, req)
}

// UpdateNews client handler
func UpdateNews(ctx context.Context, req *pb.UpdateNewsRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.UpdateNews(ctx, req)
}

// ListNews client handler
func ListNews(ctx context.Context, req *pb.ListNewsRequest) (*pb.ListNewsResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.ListNews(ctx, req)
}

// GetNews client handler
func GetNews(ctx context.Context, req *pb.GetNewsRequest) (*pb.GetNewsResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.GetNews(ctx, req)
}

// CreateBanner client handler
func CreateBanner(ctx context.Context, req *pb.CreateBannerRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.CreateBanner(ctx, req)
}

// UpdateBanner client handler
func UpdateBanner(ctx context.Context, req *pb.UpdateBannerRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.UpdateBanner(ctx, req)
}

// ListBanner client handler
func ListBanner(ctx context.Context, req *pb.ListBannerRequest) (*pb.ListBannerResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.ListBanner(ctx, req)
}

// GetBanner client handler
func GetBanner(ctx context.Context, req *pb.GetBannerRequest) (*pb.GetBannerResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.GetBanner(ctx, req)
}

// CreateWa client handler
func CreateWa(ctx context.Context, req *pb.CreateWaRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.CreateWa(ctx, req)
}

// UpdateWa client handler
func UpdateWa(ctx context.Context, req *pb.UpdateWaRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.UpdateWa(ctx, req)
}

// ListWa client handler
func ListWa(ctx context.Context, req *pb.ListWaRequest) (*pb.ListWaResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.ListWa(ctx, req)
}

// GetWa client handler
func GetWa(ctx context.Context, req *pb.GetWaRequest) (*pb.GetWaResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.GetWa(ctx, req)
}

// DeleteWa client handler
func DeleteWa(ctx context.Context, req *pb.DeleteWaRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("content_grpc"))
	defer conn.Close()

	client := pb.NewContentClient(conn)

	return client.DeleteWa(ctx, req)
}
