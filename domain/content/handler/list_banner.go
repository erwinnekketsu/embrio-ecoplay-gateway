package handler

import (
	"context"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type ListBanner struct{}

func NewListBanner() *ListBanner {
	return &ListBanner{}
}

func (h *ListBanner) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.ListBannerRequest
	adminId := c.QueryParam("admin_id")
	req.Page = int32(util.StringToInteger(c.QueryParam("page")))
	req.Status = int32(util.StringToInteger(c.QueryParam("status")))
	req.Limit = int32(util.StringToInteger(c.QueryParam("limit")))
	if adminId == "" {
		req.Status = 1
		req.Limit = 100
	}
	grpcResp, err := client.ListBanner(ctx, &req)
	if err != nil {
		log.Printf("[Nasabah] ListNasabah error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[Nasabah] ListNasabah error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *ListBanner) buildResponse(ctx context.Context, response *pb.ListBannerResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"Banner":     response.Banner,
			"pagination": response.Pagination,
		},
	}
	return resp, nil
}

func (h *ListBanner) buildErrorResponse(ctx context.Context, response *pb.ListBannerResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *ListBanner) validate(r *content.ListBannerRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
