package handler

import (
	"context"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type GetNews struct{}

func NewGetNews() *GetNews {
	return &GetNews{}
}

func (h *GetNews) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.GetNewsRequest
	req.Id = int32(util.StringToInteger(c.Param("id")))
	grpcResp, err := client.GetNews(ctx, &req)
	if err != nil {
		log.Printf("[News] ListNews error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[News] ListNews error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *GetNews) buildResponse(ctx context.Context, response *pb.GetNewsResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"news": response.News,
		},
	}
	return resp, nil
}

func (h *GetNews) buildErrorResponse(ctx context.Context, response *pb.GetNewsResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *GetNews) validate(r *content.GetNewsRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
