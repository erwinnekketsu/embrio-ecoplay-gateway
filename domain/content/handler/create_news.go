package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CreateNews struct{}

func NewCreateNews() *CreateNews {
	return &CreateNews{}
}

func (h *CreateNews) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	tokenPayload, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	r := new(content.News)
	err := h.validate(r, c)
	if err != nil {
		log.Println("validate error : ", err.Error())
		return err
	}

	var req pb.CreateNewsRequest
	bytes, _ := json.Marshal(&r)
	_ = json.Unmarshal(bytes, &req)
	req.AdminId = tokenPayload.UserID

	grpcResp, err := client.CreateNews(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := h.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (h *CreateNews) buildResponse(ctx context.Context, response *pb.NoResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
	}
	return resp, nil
}

func (h *CreateNews) buildErrorResponse(ctx context.Context, response *pb.NoResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *CreateNews) validate(r *content.News, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
