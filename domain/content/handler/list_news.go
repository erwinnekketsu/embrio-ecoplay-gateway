package handler

import (
	"context"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/auth"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content/client"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
)

type ListNews struct{}

func NewListNews() *ListNews {
	return &ListNews{}
}

func (h *ListNews) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	_, ok := c.Get(util.ContextTokenValueKey).(auth.TokenPayload)
	if !ok {
		resp := &util.Response{
			Code:    http.StatusInternalServerError,
			Message: util.StatusMessage[http.StatusInternalServerError],
			Errors:  []string{"failed load token data"},
		}
		return c.JSON(http.StatusInternalServerError, &resp)
	}

	var req pb.ListNewsRequest
	req.Page = int32(util.StringToInteger(c.QueryParam("page")))
	req.Status = int32(util.StringToInteger(c.QueryParam("status")))
	req.Limit = int32(util.StringToInteger(c.QueryParam("limit")))
	grpcResp, err := client.ListNews(ctx, &req)
	if err != nil {
		log.Printf("[News] ListNews error : %s", err.Error())
		return err
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		log.Printf("[News] ListNews error : %s", err.Error())
		return err
	}
	return resp.JSON(c)
}

func (h *ListNews) buildResponse(ctx context.Context, response *pb.ListNewsResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
		Data: map[string]interface{}{
			"news":       response.News,
			"pagination": response.Pagination,
		},
	}
	return resp, nil
}

func (h *ListNews) buildErrorResponse(ctx context.Context, response *pb.ListNewsResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *ListNews) validate(r *content.ListNewsRequest, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}
	return c.Validate(r)
}
