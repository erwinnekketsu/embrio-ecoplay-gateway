package handler

import (
	"context"
	"encoding/json"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/domain/content/client"
	pb "gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/proto/content"
	"gitlab.com/erwinnekketsu/embrio-ecoplay-gateway/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CreateWa struct{}

func NewCreateWa() *CreateWa {
	return &CreateWa{}
}

func (h *CreateWa) Handle(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	r := new(content.Wa)
	err := h.validate(r, c)
	if err != nil {
		log.Println("validate error : ", err.Error())
		return err
	}

	var req pb.CreateWaRequest
	bytes, _ := json.Marshal(&r)
	_ = json.Unmarshal(bytes, &req)

	grpcResp, err := client.CreateWa(ctx, &req)
	if err != nil {
		st, _ := status.FromError(err)
		log.Println("response", err.Error())
		resp, err := h.buildErrorResponse(ctx, grpcResp, c, st.Code(), st.Message())
		if err != nil {
			return err
		}
		return resp.JSON(c)
	}
	resp, err := h.buildResponse(ctx, grpcResp, c)
	if err != nil {
		return err
	}

	return resp.JSON(c)
}

func (h *CreateWa) buildResponse(ctx context.Context, response *pb.NoResponse, c echo.Context) (*util.Response, error) {
	resp := &util.Response{
		Code:    util.Success,
		Message: util.StatusMessage[util.Success],
	}
	return resp, nil
}

func (h *CreateWa) buildErrorResponse(ctx context.Context, response *pb.NoResponse, c echo.Context, errorCode codes.Code, message string) (*util.Response, error) {
	resp := &util.Response{
		Code:    errorCode,
		Message: util.StatusMessage[errorCode],
		Data: map[string]interface{}{
			"message": message,
		},
	}
	return resp, nil
}

func (h *CreateWa) validate(r *content.Wa, c echo.Context) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	return c.Validate(r)
}
