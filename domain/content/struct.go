package content

type News struct {
	ID        int32  `json:"id"`
	IdAdmin   int32  `json:"idAdmin"`
	Title     string `json:"title"`
	Content   string `json:"content"`
	Status    int32  `json:"status"`
	Thumbnail string `json:"thumbnail"`
	CreatedAt string `json:"createdAt"`
	UpdateAt  string `json:"updatedAt"`
}

type Banner struct {
	ID      int32  `json:"id"`
	IdAdmin int32  `json:"admin_id"`
	Name    string `json:"name"`
	Image   string `json:"image"`
	Status  int32  `json:"status"`
}

type Wa struct {
	ID     int32  `json:"id"`
	Name   string `json:"name"`
	Image  string `json:"image"`
	Link   string `json:"link"`
	Member int32  `json:"member"`
}
